//
//  BorderButton.swift
//  app-swoosh
//
//  Created by Benjamin Neale on 8/3/17.
//  Copyright © 2017 Benjamin Neale. All rights reserved.
//

import UIKit

class BorderButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        layer.borderWidth = 3.0
        layer.borderColor =
            UIColor.white.cgColor
    }

}
